# Uranus Team QA Alpha 28
# Requirements to run the tests
Download the project from https://gitlab.com/radozarev/uranus-team-project/-/tree/main

## 1. Prerequisites for running Postman tests:
- Postman Version 8.9.1
- Node.js
- Instal Newman: $ npm install -g newman

## 2. Instructions for running Postman Collections

- To execute the files please, clone the repository and run one of the batch files: 
- RunWithReportWithLocalEnv.bat - starts with Localhost environment
- RunWithReportWithOnlineEnv.bat - starts with Online deployed project

## 3. Prerequisites for running Selenium tests:
- JDK v11.0.11
- IntelliJ IDE 2021.1.1 (Community Edition)
- Apache Maven v3.8.1
- Docker Inc. 3.5.0

## 4. Instructions for running Selenium tests:
- Open the project usung IntelliJ IDE
- Open Docker desktop, here you can find the instructions for instalation: [URL](https://gitlab.com/TelerikAcademy/alpha-28-qa/-/tree/master/05.%20Final%20Project/WEare%20Docker%20yml%20version)
- Open http://localhost:8081/ in a browser
- Create 2 users: *Joey Tribbiani - Username: Joey, Password: password* and *Phoebe Buffay - Username: Phoebe, Password: password and create 1 post from Phoebe's registration*
- Navigate to 'src\test\java' in IntelliJ and run the tests 
- You can also run the test using a .bat file which you can find in TestAutomation/Framework folder
