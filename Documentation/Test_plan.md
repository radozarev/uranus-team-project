# Test plan for testing WEare Social Network

## 1. Objectives and tasks

1.1 OBJECTIVES 

Social Network web application enables users to connect with each other, create posts and comments and to get feed of the newest relevant posts of their connections.
Our main objectives are to verify that the application complies with functional requirements specified in the documenation and that the product meets
the expected quality. All main functionalities and happy paths will be tested and all bugs and suggestions found will be reported accordingly. 

1.2 TASKS

Main activities performed during project phase will be:

- Analysis of requirements listed in the documentation;
- Conducting exploratory testing in order to become acquainted with the applivation;
- Writing test cases with appropriate priority and severity;
- Perform manual and automation testing to detect usability and performance issues;
- Reports preparation;


## 2. Scope 

2.1 FEATURES TO BE TESTED

All main functionalities of the product will be tested for registered user, non-registered user and user with admin authorisation such as:

- Search for profiles by name and cathegories;

- Registration and Log in;

- Reviewing of public posts and profiles;

- Set of profile visibility;

- Interactions with other users;

- Like/Dislike functionalities;

- Post creation and adjustment;

- Comment creation and adjustment;

- Profile information update;

- Delete and edit profile/comment/post for admin;

2.2 FEATURES NOT TO BE TESTED

- What is not mentioned in the test plan is not in the scope of this project;


## 3. Approach

The testing process will include following steps:

- Analisys of the product documentation and customer's requirements;
- Performing an exploratory testing in order to get familiar with product's functionalities;
- Test cases design and implementation;
- Selenium Web Driver will be used as automation tool for performing UI testing;
- REST API testing will be performed with Postman and using the documentation from Swagger;

## 4. Test deliverables

Available deliverables after testing completion will be as follows:

- Test plan

- Test cases

- Test summary report

## 5. Responsibilities

All activities will be performed as follows:

|Task|Asignee|
|---|:---:|
|Design test cases| Both team members
|Manual execution of the created test cases|Both team members
|Test case automation|Both team members
|Summary report preparation|Both team members

## 6. Resources
|      |   |
|---|:---:|
|IDE|[Intellij](https://www.jetbrains.com/idea/)|
|Source code management tool|[GitLab](https://gitlab.com/radozarev/uranus-team-project/-/tree/main)|
|Tool for bug reporting|[GitLab](https://gitlab.com/radozarev/uranus-team-project/-/issues)|
|Rest API testing|[Postman](https://www.postman.com/)|
|UI automation testing|[Selenium WebDrive](https://www.selenium.dev/documentation/en/introduction/on_test_automation/)|

## 7. Estimation and schedule

|**Task**|**Days**|**Due date**|
|:---|:---:|---:|
|Exploratory testing| 4 days| 1 July 2021 
|Test plan|3 days| 4 July 2021 
|Tes cases design| 5 days|7 July 2021
|Manual testing| 2 days|12 July 2021
|API testing| 8 days| 20 July 2021
|UI testing| 8 days| 28 July 2021
|Bug report preparation| 1 day| 29 July 2021
|Test report preparation| 1 day|30 July 2021

## 8. Exit Criteria

- Test cases for all main functionalities listed in this plan are created

- All test cases are executed and bug report for failed cases created

- Project deadline 30 July 2021 is reached