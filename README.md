# Uranus Team QA Alpha 28

# Testing WEare Social Network

- Test cases all projectdocumentation available on: [One Drive](https://telerikacademy-my.sharepoint.com/:f:/p/nataliya_todorova_a28_learn/Etl8RMpE99JIsgeLnc9GC6MBRV1-_aHwaGHoT8wqfW1bbQ?e=JfwaqH)

- [Trello Board](https://trello.com/b/ydjTT5C3/uranus-team-board)

- Bug reports logged in  [GitLab](https://gitlab.com/radozarev/uranus-team-project/-/issues)

- Manual execution of test cases report [Manual execution report](https://telerikacademy-my.sharepoint.com/:x:/r/personal/nataliya_todorova_a28_learn_telerikacademy_com/_layouts/15/Doc.aspx?sourcedoc=%7BD81BF9FB-6937-4CA3-9658-58AE2139D2B7%7D&file=Test-case-template.xlsx&action=default&mobileredirect=true)

- [Postman report](https://gitlab.com/radozarev/uranus-team-project/-/tree/main/WEareCollection/newman)
