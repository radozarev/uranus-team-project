package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class AuthenticationPage extends BasePage {
    public AuthenticationPage(WebDriver driver) {
        super(driver, "weAre.registerPage");
    }

    public void click(String locator){
        actions.waitForElementClickableUntilTimeout(locator, 20);
        actions.resolveSVG("//div[@id= 'ftco-loader']");
        actions.clickElement(locator);
    }

    public void clickOnRegisterButton(){
        actions.clickElement("registerButtonRegisterPage");
    }

    public void typeInUsername(String term){
        actions.typeValueInField(term, "userNameField");
    }

    public void typeInEmail(String term){
        actions.typeValueInField(term, "yourEmailField");
    }

    public void typeInPasswordAndConfirm(String term){
        actions.typeValueInField(term, "passwordField");
        actions.typeValueInField(term, "passwordConfirmationField");
    }

    public void clickOnLogInButton(){
        actions.waitForElementClickableUntilTimeout("loginForm.navigationSingInButton", 20);
        actions.clickElement("loginForm.navigationSingInButton");
    }

    public void typeInUserCredentials(String username, String password){
        actions.typeValueInField(username, "logInForm.usernameField");
        actions.typeValueInField(password, "logInForm.passwordField");
    }

    public void clickOnSignInButton(){
        actions.waitForElementClickableUntilTimeout("logInForm.signInButton", 20);
        actions.resolveSVG("//div[@id= 'ftco-loader']");
        actions.clickElement("logInForm.signInButton");
    }

    public void clickOnLogOutButton(){
        actions.waitForElementVisible("logInPageLogOutButton", 50);
        actions.clickElement("logInPageLogOutButton");
    }

    public void assertLogOutButtonIsPresent(){
        actions.assertElementPresent("logInPageLogOutButton");
    }

    public void assertResultIsPresent(String resultTitle){
        actions.assertElementPresent("weAre.registerPage.banner");
    }

}
