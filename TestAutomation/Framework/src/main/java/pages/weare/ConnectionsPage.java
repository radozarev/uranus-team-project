package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class ConnectionsPage extends BasePage {

    public ConnectionsPage(WebDriver driver) {
        super(driver, "weAre.homePage");
    }

    public void click(String locator){
        actions.waitForElementClickableUntilTimeout(locator, 20);
        actions.resolveSVG("//div[@id= 'ftco-loader']");
        actions.clickElement(locator);
    }

    public void clickOnSignInButton(){
        actions.waitForElementClickableUntilTimeout("weAre.homePage.singInButton", 50);
        actions.clickElement("weAre.homePage.singInButton");
    }

    public void clickOnSeeProfileButton(){
        actions.waitForElementClickableUntilTimeout("seeProfileButton", 50);
        click("seeProfileButton");
    }

    public void clickOnPersonalProfileButton(){
        click("navigationBar.personalProfileButton");
    }

    public void clickOnConnectButton(){
        actions.waitForElementClickableUntilTimeout("connectButton", 50);
        click("connectButton");
    }

    public void clickOnDisconnectButton(){
        actions.waitForElementClickableUntilTimeout("disconnectButton", 50);
        click("disconnectButton");
    }

    public void clickOnNewFriendRequestButton(){
        actions.waitForElementClickableUntilTimeout("newFriendRequestButton", 50);
        click("newFriendRequestButton");
    }

    public void clickOnApproveFriendRequestButton(){
        actions.waitForElementClickableUntilTimeout("approveRequestButton", 50);
        actions.clickElement("approveRequestButton");
    }

    public void assertConfirmationTextIsPresent(){
        actions.waitForElementVisible("friendRequestSentSuccessfully.assertText", 50);
        actions.assertElementPresent("friendRequestSentSuccessfully.assertText");
    }

    public void assertFriendRequestConfirmationTextIsPresent(){
        actions.waitForElementVisible("friendRequestAccepted.assertText", 50);
        actions.assertElementPresent("friendRequestAccepted.assertText");
    }

    public void assertConnectButtonIsPresent(){
        actions.waitForElementVisible("connectButton", 50);
        actions.assertElementPresent("connectButton");
    }
}
