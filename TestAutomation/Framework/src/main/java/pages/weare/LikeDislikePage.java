package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class LikeDislikePage extends BasePage {
    public LikeDislikePage(WebDriver driver) {
        super(driver, "weAre.homePage");
    }

    public void click(String locator){
        actions.waitForElementClickableUntilTimeout(locator, 20);
        actions.resolveSVG("//div[@id= 'ftco-loader']");
        actions.clickElement(locator);
    }

    public void clickOnLatestPostsButton(){
        click("navigationBar.latestPostsButton");
    }

    public void clickOnLikeButton(){
        click("likeButton");
        actions.waitForElementVisible("dislikeButton", 50);
    }

    public void clickOnBrowseAllPostsButton(){
        click("browseAllPublicPosts");
    }

    public void clickOnDislikeButton(){
        click("dislikeButton");
        actions.waitForElementVisible("likeButton", 50);
    }

    public void assertDislikeButtonIsPresent(){
        actions.assertElementPresent("dislikeButton");
    }

    public void assertLikeButtonIsPresent(){
        actions.assertElementPresent("likeButton");
    }
}
