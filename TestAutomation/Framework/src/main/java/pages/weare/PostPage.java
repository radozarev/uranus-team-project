package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class PostPage extends BasePage {

    public PostPage(WebDriver driver) {
        super(driver, "weAre.homePage");
    }

    public void click(String locator){
        actions.waitForElementClickableUntilTimeout(locator, 20);
        actions.resolveSVG("//div[@id= 'ftco-loader']");
        actions.clickElement(locator);
    }

    public void clickOnAddNewPostButton(){
//        click("navigationBar.addNewPostButton");
        actions.clickElement("navigationBar.addNewPostButton");
    }


    public void clickOnSavePostButton(){
        click("createNewPost.savePostButton");
    }

    public void clickOnPersonalProfileButton(){
        click("navigationBar.personalProfileButton");
    }

    public void clickOnLastPostCreated(){
        click("personalProfile.lastPostCreated");
    }

    public void clickOnLatestPostsButton2(){
//        click("navigationBar.latestPostBtn");
//        actions.waitForElementClickableUntilTimeout("navigationBar.latestPostBtn", 20);
        click("navigationBar.latestPostBtn");
    }


    public void clickOnEditPostButton(){
        click("editPostButton");
    }

    public void clickOnDeletePostButton(){
        click("deletePostButton");
    }

    public void clickOnDeletePostSubmitButton(){
        click("deletePost.submitButton");
    }

    public void clickOnLatestPostButton(){
        click("navigationBar.latestPostsButton");
        actions.waitForElementVisible("exploreThisPostButton", 50);
    }

    public void clickOnExploreThisPostButton(){
        click("exploreThisPostButton");
    }

    public void clickOnSaveCommentButton(){
        click("createNewComment.saveCommentButton");
    }

    public void clickOnShowCommentButton(){
        click("createNewComment.showCommentsButton");
    }

    public void clickOnEditCommentButton(){
        click("createNewComment.editCommentButton");
    }

    public void clickOnEditCommentButtonAfterCommentAdjustment(){
        click("createNewComment.releaseCommentButton");
    }

    public void clickOnDeleteCommentButton(){
        click("createNewComment.deleteCommentButton");
    }

    public void clickOnDeleteCommentSubmitButton(){
        click("deleteCommentSubmitButton");
    }

    public void typeInPostText(String term){
        actions.typeValueInField(term, "createNewPost.messageField");
    }
    public void typeInCommentText(String term){
        actions.waitForElementClickableUntilTimeout("createNewComment.messageField", 20);
        actions.typeValueInField(term, "createNewComment.messageField");
    }

    public void setPostVisibility(){
        click("dropDownMenuButton");
        click("dropDownMenuPublic");
    }

    public void confirmPostDeletion(){
        click("dropDownMenuButton");
        click("dropDownMenuDeletePost");
    }

    public void confirmCommentDeletion(){
        click("dropDownMenuButtonForComments");
        click("dropDownMenuDeleteForComments");
    }

    public void assertPostIsPresent(String postText){
        actions.assertElementPresent("//p[contains(text(), '"+postText+"')]");
    }

    public void assertEditPostButtonIsPresent(){
        actions.assertElementPresent("editPostButton");
    }

    public void assertPostDeletion(){
        actions.assertElementPresent("deletePost.assertText");
    }
}
