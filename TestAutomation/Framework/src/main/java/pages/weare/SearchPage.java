package pages.weare;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class SearchPage extends BasePage {

    public SearchPage(WebDriver driver) {
        super(driver, "weAre.homePage");
    }

    public void searchByName(String term){
        actions.typeValueInField(term, "usernameSearchFiled");
    }

    public void searchAndSubmitByName(String term){
        actions.waitForElementVisible("usernameSearchFiled");
        actions.clickElement("usernameSearchFiled");
        searchByName(term);
        actions.clickElement("homePageSearchButton");
        actions.waitForElementVisible("weAre.resultsPage.firstResult");
    }

    public void searchWithSpecialCharacterAndSubmit(){
        actions.waitForElementVisible("usernameSearchFiled");
        actions.clickElement("usernameSearchFiled");
        searchByName("%$#()");
        actions.clickElement("homePageSearchButton");
    }

    public void searchByCategory(String term){
        actions.typeValueInField(term, "categorySearchField");
    }

    public void searchAndSubmitByCategory(String term){
        actions.waitForElementVisible("categorySearchField");
        actions.clickElement("categorySearchField");
        searchByCategory(term);
        actions.clickElement("homePageSearchButton");
    }

    public void assertResultIsPresent(String resultTitle){
        actions.assertElementPresent("weAre.resultsPage.firstResult");
    }
    public void assertResultForInvalidSearchIsPresent(){
        actions.assertElementPresent("invalidSearch.assertText");
    }
}
