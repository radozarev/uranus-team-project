package testCases.weare;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.weare.AuthenticationPage;

@FixMethodOrder( MethodSorters.NAME_ASCENDING )

public class AuthenticationTests extends BaseTest {

    AuthenticationPage authenticate = new AuthenticationPage(actions.getDriver());

    private String Username = "BruceAlmighty";
    private String Email = "bruce@eoopy.com";
    private String Password = "qqq123";

    @Before
    public void navigateToPage(){
        authenticate.navigateToPage();
    }

    @Test
    public void stage001_registerNewUser_001(){
        authenticate.typeInUsername(Username);
        authenticate.typeInEmail(Email);
        authenticate.typeInPasswordAndConfirm(Password);
        actions.scrollPage();
        authenticate.clickOnRegisterButton();

        authenticate.assertResultIsPresent("weAre.registerPage.banner");
    }

    @Test
    public void stage002_logIn_when_userAccountIsCreated_002(){
        authenticate.clickOnLogInButton();
        authenticate.typeInUserCredentials(Username, Password);
        authenticate.clickOnSignInButton();

        authenticate.assertLogOutButtonIsPresent();
        authenticate.clickOnLogOutButton();
    }

    @Test
    public void stage003_logOut_when_userLoggedInSuccessfully_003(){
        authenticate.clickOnLogInButton();
        authenticate.typeInUserCredentials(Username, Password);
        authenticate.clickOnSignInButton();
        authenticate.clickOnLogOutButton();
        actions.waitFor(2000);

        actions.assertElementPresent("logOutAssertText");
    }

}
