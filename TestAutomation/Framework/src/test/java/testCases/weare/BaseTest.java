package testCases.weare;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {
	UserActions actions = new UserActions();
	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser("weAre.homePage");
	}

	public void logIn() {
		actions.waitForElementClickableUntilTimeout("weAre.homePage.singInButton", 20);
		actions.resolveSVG("//div[@id= 'ftco-loader']");
		actions.clickElement("weAre.homePage.singInButton");
		actions.waitForElementClickableUntilTimeout("logInForm.usernameField", 20);
		actions.typeValueInField("Phoebe", "logInForm.usernameField");
		actions.typeValueInField("password", "logInForm.passwordField");
		actions.resolveSVG("//div[@id= 'ftco-loader']");
		actions.clickElement("logInForm.signInButton");
	}

	public void logOut() {
		actions.waitForElementClickableUntilTimeout("homeButton", 20);
		actions.resolveSVG("//div[@id= 'ftco-loader']");
		actions.clickElement("homeButton");
		actions.waitForElementClickableUntilTimeout("logOutButton", 20);
		actions.resolveSVG("//div[@id= 'ftco-loader']");
		actions.clickElement("logOutButton");
		actions.waitFor(2000);
		actions.waitForElementClickableUntilTimeout("homeButton", 20);
		actions.resolveSVG("//div[@id= 'ftco-loader']");
		actions.clickElement("homeButton");
		actions.waitFor(2000);
		actions.waitForElementClickableUntilTimeout("weAre.homePage.singInButton", 20);
	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
}
