package testCases.weare;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.weare.LikeDislikePage;
import pages.weare.PostPage;

@FixMethodOrder( MethodSorters.NAME_ASCENDING )

public class CommentsTests extends BaseTest {

    PostPage post = new PostPage(actions.getDriver());
    LikeDislikePage activity = new LikeDislikePage(actions.getDriver());
    private String commentText = "Text";
    private String updatedCommentText = "Text adjusted five";
    private String postText = "New post created";

    @Before
    public void logIn() {
        super.logIn();
    }

    @Test
    public void stage001_createComment_when_userIsLoggedIn_001(){
        post.clickOnAddNewPostButton();
        post.setPostVisibility();
        post.typeInPostText(postText);
        post.clickOnSavePostButton();
        post.clickOnLatestPostsButton2();
        post.clickOnExploreThisPostButton();
        actions.scrollPage();
        post.typeInCommentText(commentText);
        post.clickOnSaveCommentButton();
        post.clickOnShowCommentButton();
        actions.waitFor(2000);
        actions.assertElementPresent("createNewComment.presentName");
    }

    @Test
    public void stage002_editComment_when_userIsLoggedIn_002(){
        post.clickOnLatestPostButton();
        post.clickOnExploreThisPostButton();
        actions.waitFor(1000);
        actions.scrollPage();
        post.clickOnShowCommentButton();
        actions.scrollPageDown();
        post.clickOnEditCommentButton();
        actions.waitFor(1000);
        post.typeInCommentText(updatedCommentText);
        actions.waitFor(500);
        post.clickOnEditCommentButtonAfterCommentAdjustment();
        actions.scrollPageUp();
        post.clickOnShowCommentButton();

        actions.assertElementText("editedTestLocator", updatedCommentText);
    }

    @Test
    public void stage003_deleteComment_when_userIsLoggedIn_003(){
        post.clickOnLatestPostButton();
        post.clickOnExploreThisPostButton();
        actions.waitFor(1000);
        actions.scrollPage();
        post.clickOnShowCommentButton();
        actions.scrollPageDown();
        post.clickOnDeleteCommentButton();
        actions.scrollPageDown();
        post.confirmCommentDeletion();
        post.clickOnDeleteCommentSubmitButton();
        actions.waitFor(500);
        actions.assertElementPresent("commentDeletionBanner");
    }

    @After
    public void logOut() {
        super.logOut();
    }

}
