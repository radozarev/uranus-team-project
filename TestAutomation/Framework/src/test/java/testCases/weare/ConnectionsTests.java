package testCases.weare;

import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.weare.AuthenticationPage;
import pages.weare.ConnectionsPage;
import pages.weare.SearchPage;

@FixMethodOrder( MethodSorters.NAME_ASCENDING )

public class ConnectionsTests extends BaseTest{

    ConnectionsPage connection = new ConnectionsPage(actions.getDriver());
    SearchPage search = new SearchPage(actions.getDriver());
    AuthenticationPage authenticate = new AuthenticationPage(actions.getDriver());
    private String searchedName = "Joey Tribbiani";
    private String User1 = "Phoebe";
    private String Password = "password";
    private String User2 = "Joey";

    @Test
    public void stage001_sendFriendRequest_when_userIsLoggedIn_001(){
        connection.clickOnSignInButton();
        authenticate.typeInUserCredentials(User1, Password);
        authenticate.clickOnSignInButton();
        search.searchAndSubmitByName(searchedName);
        connection.clickOnSeeProfileButton();
        connection.clickOnConnectButton();

        connection.assertConfirmationTextIsPresent();
    }

    @Test
    public void stage002_acceptFriendRequest_when_userIsLoggedIn_002(){
        connection.clickOnSignInButton();
        authenticate.typeInUserCredentials(User2, Password);
        authenticate.clickOnSignInButton();
        connection.clickOnPersonalProfileButton();
        connection.clickOnNewFriendRequestButton();
        connection.clickOnApproveFriendRequestButton();

        connection.assertFriendRequestConfirmationTextIsPresent();
    }

    @Test
    public void stage003_disconnectFriend_when_userIsLoggedIn_003(){
        connection.clickOnSignInButton();
        authenticate.typeInUserCredentials(User1, Password);
        authenticate.clickOnSignInButton();
        search.searchAndSubmitByName(searchedName);
        connection.clickOnSeeProfileButton();
        connection.clickOnDisconnectButton();

        connection.assertConnectButtonIsPresent();
    }

    @After
    public void logOut() {
        super.logOut();
    }
}
