package testCases.weare;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.weare.LikeDislikePage;
import pages.weare.PostPage;

@FixMethodOrder( MethodSorters.NAME_ASCENDING )

public class LikeDislikeTests extends BaseTest{

    LikeDislikePage activity = new LikeDislikePage(actions.getDriver());
    PostPage post = new PostPage(actions.getDriver());
    private String postText = "New post created";
    @Before
    public void logIn() {
        super.logIn();
    }

    @Test
    public void stage001_likePost_when_userIsLoggedIn(){
        activity.clickOnLatestPostsButton();
        actions.waitFor(2000);
        activity.clickOnBrowseAllPostsButton();
        actions.scrollPageDown();
        activity.clickOnLikeButton();

        activity.assertDislikeButtonIsPresent();
    }
    @Test
    public void stage002_dislikePost_when_userIsLoggedIn(){
        activity.clickOnLatestPostsButton();
        actions.waitFor(2000);
        activity.clickOnBrowseAllPostsButton();
        actions.scrollPageDown();
        activity.clickOnDislikeButton();

        activity.assertLikeButtonIsPresent();
    }

    @After
    public void logOut() {
        super.logOut();
    }
}
