package testCases.weare;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.weare.PostPage;

@FixMethodOrder( MethodSorters.NAME_ASCENDING )

public class PostTests extends BaseTest {

    PostPage post = new PostPage(actions.getDriver());
    private String postText = "New post created";
    private String updatedPostText = postText + " - adjusted";

    @Before
    public void logIn() {
        super.logIn();
    }

    @Test
    public void stage001_createPost_when_userIsLoggedIn_001(){
        post.clickOnAddNewPostButton();
        post.setPostVisibility();
        post.typeInPostText(postText);
        post.clickOnSavePostButton();
        actions.waitForElementClickableUntilTimeout("createNewPost.assertText", 20);
        post.assertPostIsPresent(postText);
    }

    @Test
    public void stage002_editPost_when_userIsLoggedIn_002(){
        post.clickOnPersonalProfileButton();
        actions.scrollPage();
        post.clickOnLastPostCreated();
        post.clickOnEditPostButton();
        post.setPostVisibility();
        post.typeInPostText(updatedPostText);
        post.clickOnSavePostButton();
        actions.waitForElementClickableUntilTimeout("editPostButton", 20);
//        actions.assertElementPresent("editPostButton");
        post.assertEditPostButtonIsPresent();
        post.assertPostIsPresent(updatedPostText);
    }

    @Test
    public void stage003_deletePost_when_userIsLoggedIn_003(){
        post.clickOnPersonalProfileButton();
        actions.waitForElementClickableUntilTimeout("personalProfile.assertText", 20);
        actions.scrollPage();
        post.clickOnLastPostCreated();
        post.clickOnDeletePostButton();
        post.confirmPostDeletion();
        post.clickOnDeletePostSubmitButton();
        post.assertPostDeletion();
    }

    @After
    public void logOut() {
        super.logOut();
    }

}
