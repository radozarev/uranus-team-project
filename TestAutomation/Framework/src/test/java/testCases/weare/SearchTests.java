package testCases.weare;

import org.junit.Before;
import org.junit.Test;
import pages.weare.SearchPage;

public class SearchTests extends BaseTest {

    SearchPage search = new SearchPage(actions.getDriver());

    private String searchedName = "Joey Tribbiani";
    private String searchedCategory = "Actor";

    @Before
    public void navigateToPage(){
        search.navigateToPage();
    }

    @Test
    public void searchResultsVisible_when_termIsSearchedByName(){
        search.searchAndSubmitByName(searchedName);
        search.assertResultIsPresent(searchedName);
    }

    @Test
    public void searchResultsVisible_when_termIsSearchedByCategory(){
        search.searchAndSubmitByCategory(searchedCategory);
        search.assertResultIsPresent(searchedName);
    }

    @Test
    public void searchResultsVisible_when_searchTermIncludeSpecialCharacter(){
        search.searchWithSpecialCharacterAndSubmit();
        search.assertResultForInvalidSearchIsPresent();
    }
}
